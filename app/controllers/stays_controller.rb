# @class StaysController
# @author Evan
class StaysController < ApplicationController
  def index
    @stays = Stay.all
  end

  def show
    @stay = Stay.find(params[:id])
  end
end
