# @class Company
# @author Evan
class Company < ActiveRecord::Base
  has_many :properties
end
