# @class Company
# @author Evan
#
# Property models represents a physical building
#
class Property < ActiveRecord::Base
  belongs_to :company

  has_many :stays

  def current_stays
    stays.on_date(Date.today)
  end
end
